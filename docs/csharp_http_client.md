## C# HTTP Client Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new HttpClient();
var request = new HttpRequestMessage
{
    Method = HttpMethod.Put,
    RequestUri = new Uri("http://localhost:9001/byte/dir/filename.ext"),
    Headers =
    {
        { "authorization", "Bearer super_secret" },
    },
    Content = binary_file,
    {
        Headers =
        {
            ContentType = new MediaTypeHeaderValue("application/octet-stream")
        }
    }
};
using (var response = await client.SendAsync(request))
{
    response.EnsureSuccessStatusCode();
    var body = await response.Content.ReadAsStringAsync();
    Console.WriteLine(body);
}
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new HttpClient();
var request = new HttpRequestMessage
{
    Method = HttpMethod.Put,
    RequestUri = new Uri("http://localhost:9001/base64/dir/filename.ext"),
    Headers =
    {
        { "authorization", "Bearer super_secret" },
    },
    Content = new StringContent("base64content"),
    {
        Headers =
        {
            ContentType = new MediaTypeHeaderValue("application/octet-stream")
        }
    }
};
using (var response = await client.SendAsync(request))
{
    response.EnsureSuccessStatusCode();
    var body = await response.Content.ReadAsStringAsync();
    Console.WriteLine(body);
}
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new HttpClient();
var request = new HttpRequestMessage
{
    Method = HttpMethod.Delete,
    RequestUri = new Uri("http://localhost:9001/delete/dir/filename.ext"),
    Headers =
    {
        { "authorization", "Bearer super_secret" },
    },
};
using (var response = await client.SendAsync(request))
{
    response.EnsureSuccessStatusCode();
    var body = await response.Content.ReadAsStringAsync();
    Console.WriteLine(body);
}
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```csharp
var client = new HttpClient();
var request = new HttpRequestMessage
{
    Method = HttpMethod.Get,
    RequestUri = new Uri("http://localhost:9001/files/dir/filename.ext"),
};
using (var response = await client.SendAsync(request))
{
    response.EnsureSuccessStatusCode();
    var body = await response.Content.ReadAsStringAsync();
    Console.WriteLine(body);
}
```
## RestSharp Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new RestClient("http://localhost:9001/byte/dir/filename.ext");
var request = new RestRequest(Method.PUT);
request.AddHeader("content-type", "application/octet-stream");
request.AddHeader("authorization", "Bearer super_secret");
request.AddParameter("application/octet-stream", binary_file, ParameterType.RequestBody);
IRestResponse response = client.Execute(request);
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new RestClient("http://localhost:9001/base64/dir/filename.ext");
var request = new RestRequest(Method.PUT);
request.AddHeader("content-type", "image/jpeg");
request.AddHeader("authorization", "Bearer super_secret");
request.AddParameter("image/jpeg", "base64content", ParameterType.RequestBody);
IRestResponse response = client.Execute(request);
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```csharp
var client = new RestClient("http://localhost:9001/delete/dir/filename.ext");
var request = new RestRequest(Method.DELETE);
request.AddHeader("authorization", "Bearer super_secret");
IRestResponse response = client.Execute(request);
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```csharp
var client = new RestClient("http://localhost:9001/files/dir/filename.ext");
var request = new RestRequest(Method.GET);
IRestResponse response = client.Execute(request);
```
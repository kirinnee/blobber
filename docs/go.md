## Go Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```go
package main

import (

"fmt"
"io/ioutil"
"net/http"
"strings"
)

func main() {

	url := "http://localhost:9001/byte/dir/filename.ext"

	payload := some_byte_array

	req, _ := http.NewRequest("PUT", url, payload)

	req.Header.Add("content-type", "application/octet-stream")
	req.Header.Add("authorization", "Bearer super_secret")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```go
package main

import (

"fmt"
"io/ioutil"
"net/http"
"strings"
)

func main() {

	url := "http://localhost:9001/base64/dir/filename.ext"

	payload := strings.NewReader("base64content")

	req, _ := http.NewRequest("PUT", url, payload)

	req.Header.Add("content-type", "image/jpeg")
	req.Header.Add("authorization", "Bearer super_secret")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```go
package main

import (

"fmt"
"io/ioutil"
"net/http"
)

func main() {

	url := "http://localhost:9001/delete/dir/filename.ext"

	req, _ := http.NewRequest("DELETE", url, nil)

	req.Header.Add("authorization", "Bearer super_secret")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```go
package main

import (

"fmt"
"io/ioutil"
"net/http"
)

func main() {

	url := "http://localhost:9001/files/dir/filename.ext"

	req, _ := http.NewRequest("GET", url, nil)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
```
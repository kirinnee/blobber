## Javascript Http (Node) Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```js
const http = require("http");

const options = {
  "method": "PUT",
  "hostname": "localhost",
  "port": "9001",
  "path": "/byte/dir/filename.ext",
  "headers": {
    "content-type": "application/octet-stream",
    "content-length": "0",
    "authorization": "Bearer super_secret"
  }
};

const req = http.request(options, (res) => {
  const chunks = [];

  res.on("data",  (chunk) => {
    chunks.push(chunk);
  });

  res.on("end", () => {
    const body = Buffer.concat(chunks);
    console.log(body.toString());
  });
});

req.write(binary_file);
req.end();
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```js
var http = require("http");

var options = {
  "method": "PUT",
  "hostname": "localhost",
  "port": "9001",
  "path": "/base64/dir/filename.ext",
  "headers": {
    "content-type": "image/jpeg",
    "content-length": "13",
    "authorization": "Bearer super_secret"
  }
};

const req = http.request(options,  (res) => {
  const chunks = [];

  res.on("data",  (chunk) => {
    chunks.push(chunk);
  });

  res.on("end",  () => {
    const body = Buffer.concat(chunks);
    console.log(body.toString());
  });
});

req.write("base64content");
req.end();
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```js
const http = require("http");

const options = {
  "method": "DELETE",
  "hostname": "localhost",
  "port": "9001",
  "path": "/delete/dir/filename.ext",
  "headers": {
    "content-length": "0",
    "authorization": "Bearer super_secret"
  }
};

const req = http.request(options, (res) => {
  const chunks = [];

  res.on("data", (chunk) => {
    chunks.push(chunk);
  });

  res.on("end", () => {
    const body = Buffer.concat(chunks);
    console.log(body.toString());
  });
});

req.end();
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```js
const http = require("http");

const options = {
  "method": "GET",
  "hostname": "localhost",
  "port": "9001",
  "path": "/files/dir/filename.ext",
};

const req = http.request(options, (res) => {
  const chunks = [];

  res.on("data",  (chunk) => {
    chunks.push(chunk);
  });

  res.on("end",  () => {
    const body = Buffer.concat(chunks);
    console.log(body.toString());
  });
});

req.end();
```
## Javascript Request (Node) Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```js
const request = require("request");

const options = {
  method: 'PUT',
  url: 'http://localhost:9001/byte/dir/filename.ext',
  headers: {
    'content-type': 'application/octet-stream',
    authorization: 'Bearer super_secret'
  },
  body: binary_file
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```js
const request = require("request");

const options = {
  method: 'PUT',
  url: 'http://localhost:9001/base64/dir/filename.ext',
  headers: {'content-type': 'image/jpeg', authorization: 'Bearer super_secret'},
  body: 'base64content'
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```js
const request = require("request");

const options = {
  method: 'DELETE',
  url: 'http://localhost:9001/delete/dir/filename.ext',
  headers: {authorization: 'Bearer super_secret'}
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```js
const request = require("request");

const options = {method: 'GET', url: 'http://localhost:9001/files/dir/filename.ext'};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```
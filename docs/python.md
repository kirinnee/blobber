## Python Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```python
import http.client

conn = http.client.HTTPConnection("localhost:9001")

payload = binary_file

headers = {
    'content-type': "application/octet-stream",
    'authorization': "Bearer super_secret"
    }

conn.request("PUT", "/byte/dir/filename.ext", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```python
import http.client

conn = http.client.HTTPConnection("localhost:9001")

payload = base64content

headers = {
    'content-type': "image/jpeg",
    'authorization': "Bearer super_secret"
    }

conn.request("PUT", "/base64/dir/filename.ext", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```python
import http.client

conn = http.client.HTTPConnection("localhost:9001")

payload = ""

headers = { 'authorization': "Bearer super_secret" }

conn.request("DELETE", "/delete/dir/filename.ext", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```python
import http.client

conn = http.client.HTTPConnection("localhost:9001")

payload = ""

conn.request("GET", "/files/dir/filename.ext", payload)

res = conn.getresponse()
data = res.read()

```
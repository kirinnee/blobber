## Ruby Examples

In these examples, we will assume we set the password to `super_secret`, 
and we want to add the file `filename.ext` under the folder `dir`. 


#### Posting with Raw Binary

Method: PUT 

EndPoint: `/byte/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```ruby
require 'uri'
require 'net/http'

url = URI("http://localhost:9001/byte/dir/filename.ext")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Put.new(url)
request["content-type"] = 'application/octet-stream'
request["authorization"] = 'Bearer super_secret'
request.body = binary_file

response = http.request(request)
puts response.read_body
```

#### Posting with Base64

Method: PUT 

EndPoint: `/base64/:dir/:file`

`:dir`  and `:file` can only contain alphanumeric, hyphens, underscores and dots. Creating multi nested 
folders is not supported. 

This requires Bearer Token if the `BLOBBER_PW` is set

```ruby
require 'uri'
require 'net/http'

url = URI("http://localhost:9001/base64/dir/filename.ext")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Put.new(url)
request["content-type"] = 'image/jpeg'
request["authorization"] = 'Bearer super_secret'
request.body = "base64content"

response = http.request(request)
puts response.read_body
```

#### Deleting

Method: DELETE 

EndPoint: `/delete/:dir/:file`

This requires Bearer Token if the `BLOBBER_PW` is set

```ruby
require 'uri'
require 'net/http'

url = URI("http://localhost:9001/delete/dir/filename.ext")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Delete.new(url)
request["authorization"] = 'Bearer super_secret'

response = http.request(request)
puts response.read_body
```

#### Accessing the file for download

Method: GET 

Endpoint: `/files/:dir/:file` 

This does not require any authentication. This endpoint is public.

```ruby
require 'uri'
require 'net/http'

url = URI("http://localhost:9001/files/dir/filename.ext")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)

response = http.request(request)
puts response.read_body
```
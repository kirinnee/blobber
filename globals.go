package main

import (
	"os"
	"time"
)

var status = ServerStatus{
	Status:       "Ok",
	Started:      time.Now(),
	RequestCount: 0,
}

var port = "9001"
var directory = "/blobs"
var password = ""
var cors = ""

func LoadEnv() {
	port = os.Getenv("BLOBBER_PORT")
	directory = os.Getenv("BLOBBER_DIR")
	password = os.Getenv("BLOBBER_PW")
	cors = os.Getenv("BLOBBER_CORS")
	if port == "" {
		port = "9001"
	}
	if directory == "" {
		directory = "/blobs"
	}
}

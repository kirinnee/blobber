module BlobServer

go 1.14

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20200810150920-a32d7af194d1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/h2non/filetype v1.1.0
	gitlab.com/kiringo/goatling v0.0.9
)

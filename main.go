package main

import (
	"encoding/base64"
	"flag"
	"github.com/h2non/filetype"
	g "gitlab.com/kiringo/goatling"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

// Sample Endpoint
func StatusCheck(goat g.Goat) *g.ServerResponse {
	status.RequestCount += 1
	return g.OK(status)
}

func AddFileBase64(goat g.Goat) *g.ServerResponse {
	status.RequestCount += 1
	if password != "" && goat.Header().Get("Authorization") != "Bearer "+password {
		return g.Unauthorized("Incorrect Password")
	}
	dir := goat.Vars()["dir"]
	path := goat.Vars()["path"]
	d := filepath.Join(directory, dir)
	p := filepath.Join(d, path)

	Content := goat.BodyBytes()
	content, errD := base64.StdEncoding.DecodeString(string(Content))

	if errD != nil {
		return g.BadRequest("Failed to decode content: " + errD.Error())
	}

	errE := os.MkdirAll(d, os.ModePerm)
	if errE != nil {
		return g.BadRequest("Failed to create directory: " + errE.Error())

	}
	err := ioutil.WriteFile(p, content, 0644)
	if err != nil {
		return g.BadRequest("Failed to write file: " + err.Error())
	}
	m, err2 := filetype.Match(content)
	if err2 != nil {
		return g.BadRequest("Mime check failed: " + err2.Error())
	}
	return g.OK(BlobResponse{
		Link: filepath.Join(dir, path),
		Mime: m.MIME.Value,
	})
}

func AddFileByte(goat g.Goat) *g.ServerResponse {
	status.RequestCount += 1

	if password != "" && goat.Header().Get("Authorization") != "Bearer "+password {
		return g.Unauthorized("Incorrect Password")
	}

	dir := goat.Vars()["dir"]
	path := goat.Vars()["path"]

	d := filepath.Join(directory, dir)
	p := filepath.Join(d, path)

	Content := goat.BodyBytes()

	errE := os.MkdirAll(d, os.ModePerm)
	if errE != nil {
		return g.BadRequest("Failed to create directory: " + errE.Error())

	}
	err := ioutil.WriteFile(p, Content, 0644)
	if err != nil {
		return g.BadRequest("Failed to write file: " + err.Error())
	}
	var m, err2 = filetype.Match(Content)
	if err2 != nil {
		return g.BadRequest("Mime check failed: " + err2.Error())
	}
	return g.OK(BlobResponse{
		Link: filepath.Join(dir, path),
		Mime: m.MIME.Value,
	})
}

func DeleteFile(goat g.Goat) *g.ServerResponse {
	status.RequestCount += 1
	if password != "" && goat.Header().Get("Authorization") != "Bearer "+password {
		return g.Unauthorized("Incorrect Password")
	}

	dir := goat.Vars()["dir"]
	path := goat.Vars()["path"]

	p := filepath.Join(directory, dir, path)
	if PathExist(p) {
		err := os.Remove(p)
		if err != nil {
			return g.BadRequest(err.Error())
		}
		return g.NoContent(nil)
	}
	return g.NotFound([]byte("Cannot find blob"))

}

func GetFile(goat g.Goat) *g.ServerResponse {
	status.RequestCount += 1
	dir := goat.Vars()["dir"]
	path := goat.Vars()["path"]
	p := filepath.Join(directory, dir, path)
	if PathExist(p) {

		file, err2 := ioutil.ReadFile(p)
		if err2 != nil {
			return g.BadRequest("Failed to read file: " + err2.Error())
		}
		kind, _ := filetype.Match(file)
		goat.Header().Set("Content-Type", kind.MIME.Value)
		return g.OK(file)
	}
	return g.NotFound([]byte("Cannot find blob"))

}

func main() {

	LoadEnv()

	var addr = flag.String("addr", ":"+port, "http service address")
	flag.Parse()

	newpath := directory
	err := os.MkdirAll(newpath, os.ModePerm)
	if err != nil {
		log.Fatal("Failed to initialize storage")
	}

	// Normal Server
	r := g.New()

	if cors != "" {
		r.SetCORS(cors)
	}
	// Now you can use r and a like gorilla mux server
	r.Serve("/byte/{dir}/{path}", AddFileByte).Methods("PUT")
	r.Serve("/base64/{dir}/{path}", AddFileBase64).Methods("PUT")
	r.Serve("/delete/{dir}/{path}", DeleteFile).Methods("DELETE")
	r.ServeRaw("/files/{dir}/{path}", GetFile).Methods("GET")
	r.Serve("/", StatusCheck).Methods("GET")

	log.Println("Listening port: " + port)
	log.Fatal(http.ListenAndServe(*addr, r))
}

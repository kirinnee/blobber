package main

import "time"

type (
	ServerStatus struct {
		Status       string
		Started      time.Time
		RequestCount uint64
	}

	BlobResponse struct {
		Link string
		Mime string
	}
)
